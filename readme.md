## About

A small bash script implementing a basic Dictaphone, designed for
taking notes while reading books.  Highly personalised for my setup,
provided here mainly as a suggestion for personal implementation.

Why *another* recording program?  Because taking notes is too
difficult already---one has to turn around, reach for the pencil,
support the paper with the other hand... typing?  then I’d have to put
the book down.  If one had a smartphone there are simple
‘tap-to-record’ apps; this is something similar on the terminal.  Why
‘+’?  Because it’s on the extreme right of the number-pad, and easily
accessible---or you can map a foot-pedal to it.  The idea is that note
taking should not involve anything more than pressing a button and
saying something.

There are almost certainly other programs out there doing something
similar, but it was more hassle to look for one than to code my
own---plus is wasn’t nearly such good procrastination ;)

## Usage

Select directory---this defaults to the folder in which I store notes,
taken with `Emacs` and `Helm-Bibtex`.  Press `+` to start recording,
`+` again to stop.  Each dictation will be saved as an ogg file in the
target folder; then you can play them easily with dired or the like
while transcribing.  The terminal bell is triggered on the end of
every recording.  If you find this annoying, comment out the line
`tput bel`.

When setting levels it can be handy to play the previous
recording---press `-` and it will be played with `ogg123`.

Occasionally one wishes to start over: `/` will delete the previous
recording, though the counter will not be reset unless you close and
re-open the script.

If running in gnome the script will disable screenlock for the
duration of runtime (just close and re-open if you want to go
away....).  This is repaired if exited cleanly, but if you `kill -9`
you’ll need to set the schema yourself.

N.B. that this program is *not* designed for extensive recording (use
a real sound recorder for that, like Audacity!) and uses `.wav`
temporary files in the target directory, so if you leave it running
for hours it will fill up surprisingly quickly.

## Installation

Clone the repository locally, make sure `zenity`, `arecord` and
`oggenc` are installed; symlink `dictaphone.sh` somewhere in your
path, and then run from a terminal with `dictaphone.sh`.

## License

Gnu GPL
