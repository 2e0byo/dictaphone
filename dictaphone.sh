#!/bin/bash
# A small script to provide dictaphone service for note-taking on books.

# Enters an infinite loop where recording short notes is possible by
# pressing enter, pressing enter again to stop.  Notes are saved in a
# temporary directory, by default in ~/Dropbox/library/notes/
wdir="${HOME}/Dropbox/library/notes/"

# Depends upon Zenity for file-manager, arecord and oggenc to record.
dep=( zenity arecord oggenc ogg123 )

# No playback support: use emacs itself whilst transcribing... ;)
function finish {		# cleanup on exit
    if [ $gdm = "true" ]	# set idle-delay back if needed
    then
	gsettings set org.gnome.desktop.session idle-delay $idle_delay
    fi

}
trap finish EXIT



# Check for dependencies
for i in "${dep[@]}"; do
    if ! which $i > /dev/null
    then echo "Please Install $i!"
	 exit
    fi
done

# get sensible wdir
wdir="$(zenity --file-selection --directory --filename=$wdir)"

cd $wdir
pwd

# disable screen lock if in gnome
if ( ps -A | grep gdm3 > /dev/null ) # running gdm3
then
    gdm=true
    idle_delay=$( gsettings get org.gnome.desktop.session idle-delay | awk '{print$2}' )
    gsettings set org.gnome.desktop.session idle-delay 0
else
    gdm=false
fi


# main loop--file saved name is 'dd-mm-yy_nnn.ogg'
suff=1
pref=$(date +%d-%m-%y)
suff=$(printf %03d $suff) # zeropad
fname="$pref"_"$suff.ogg"

while true; do
    pref=$(date +%d-%m-%y)	# repeat in case taking notes at midnight... well, it could happen!
    
    while [ -e $fname -o -e $fname.wav ]; do		# Increment name till unique
	#	let "suff=$suff + 1"
	suff=$((10#$suff))		 # strip leading zeros (or taken as octal)
	suff=$(printf %03d $((suff+1)) ) # zeropad
	fname="$pref"_"$suff.ogg"
    done
    
    read -p "Press + to start/stop recording, - to play and / to delete previous recording, and q to exit  " -n 1 act

    case $act in
	q)
	    echo ""		# tidy up with newline
	    exit
	    ;;
	+)
	    echo "$fname"
	    { (arecord -f S16_LE -c1 -r 44100 $fname.wav) & } 2>/dev/null
	    rpid=$!
	    echo -e "\nPress + to stop recording"
	    while true			# wait for '+' to exit
	    do
		read -t 0.1 -n 1  act
		if [[ $act = '+' ]]
		then
		    { kill $rpid; } 2>/dev/null # exit
		    { ( oggenc $fname.wav -o $fname && rm $fname.wav ) & } 2>/dev/null # encode
		    tput bel	# ring bell
		    last=$fname	# last recording
		    break
		fi
	    done
	    ;;
	-)
	    ogg123 $last
	    ;;
	/)			# we assume encoding has finished: a fair assumption
	    rm $last.wav	       # if exists
	    rm $last
	    ;;
    esac
done
